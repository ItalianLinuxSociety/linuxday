<?php

date_default_timezone_set("Europe/Rome");
$current_year = '2017';
$computer_date = '2017-10-28';
$shipping_date = '2017-10-28';
$human_date = 'Sabato 28 Ottobre 2017';
$administrators = ['bob@linux.it'];

$is_virtual = false;
$is_physical = true;
$sessions = [];
$talks_date = null;

$sponsors = [
    'Linux Professional Institute Italia' => (object) [
        'logo' => '/immagini/lpi.png',
        'link' => 'https://www.lpi.org/it/',
    ],
];

$supporters = [];
$patronages = [];

$theme = [];
