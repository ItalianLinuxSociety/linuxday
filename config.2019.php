<?php

date_default_timezone_set("Europe/Rome");
$current_year = '2019';
$computer_date = '2019-10-26';
$shipping_date = '2019-10-11';
$human_date = 'Sabato 26 Ottobre 2019';
$administrators = ['bob@linux.it', 'ferdi.traversa@gmail.com'];

$is_virtual = false;
$is_physical = true;
$sessions = [];
$talks_date = null;

$sponsors = [
    'Linux Professional Institute Italia' => (object) [
        'logo' => '/immagini/lpi.png',
        'link' => 'https://www.lpi.org/it/',
    ],
];

$supporters = [];
$patronages = [];

$theme = [
    "Artificial Intelligence, Machine Learning, Big Data... Utili strumenti che ci semplificano la vita, ma anche minacce per la privacy e l'individualità.",
    "Parliamone insieme al Linux Day! Perché nel mondo reale, contrariamente al mondo digitale, non è tutto 0 o 1..."
];
