<?php
/*
Copyright (C) 2019-2024  Italian Linux Society, Linux Day website contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once ('funzioni.php');

lugheader ('Linux Day ' . conf('current_year'),
    [makeurl('/libraries/leaflet/leaflet.css'), makeurl('/libraries/leaflet/leaflet-gesture-handling.css'), makeurl('/registra/registra.css')],
    [makeurl('/libraries/leaflet/leaflet.js'), makeurl('/libraries/leaflet/leaflet-gesture-handling.js')]
);

$year = conf('current_year');

// Parse the Linux Day(s) dataset. Ready to query.
$collection = new LinuxDayEventCollection();

// Get all the approved events, in a nice order.
$events =
	$collection->query()
	->whereIsApproved()
	->orderByDefault()
	->get();

// Get just the events that start before Saturday.
$early_events =
	$collection->query()
	->whereIsApproved()
	->whereHasEarlyDate()
	->orderByDefault()
	->get();

// Get just the events that start after Saturday.
$late_events =
	$collection->query()
	->whereIsApproved()
	->whereHasLateDate()
	->orderByDefault()
	->get();

// Get just the events that start exactly on Saturday.
$normal_events =
	$collection->query()
	->whereIsApproved()
	->whereHasTraditionalDate()
	->orderByDefault()
	->get();
?>

<?php if ($year == 2021 || $year == 2022): ?>
    <?php

    $running_c4p = $year == date('Y') && true;

    $sessions = conf('sessions');
    $active_lives = false;

    foreach($sessions as $session_slug => $session_meta) {
        if ($session_meta->live) {
            $active_lives = true;
            break;
        }
    }

    ?>

    <?php if ($active_lives): ?>
        <div class="row mb-5">
            <div class="col-12 text-center mb-3">
                <p class="lead">SIAMO ONLINE</p>
            </div>

            <?php foreach($sessions as $session_slug => $session_meta): ?>
                <?php if ($session_meta->live): ?>
                    <div class="col-6">
                        <div class="alert alert-info text-center">
                            <a href="<?php echo makeurl('programma/live.php?slug=' . $session_slug) ?>">
                                Clicca qui per la sessione live:<br><strong><?php echo $session_meta->label ?></strong>
                            </a>
                        </div>
                    </div>
                <?php endif ?>
            <?php endforeach ?>

            <div class="col-12">
                <div class="alert alert-info text-center">
                    <a href="<?php echo makeurl('programma') ?>">
                        Clicca qui per il programma completo.
                    </a>
                </div>
            </div>
        </div>
    <?php endif ?>

    <?php if($running_c4p && date('Y-m-d') < conf('talks_date')): ?>
        <div class="alert alert-info call4papers row mb-5 ml-5 mr-5 p-5">
            <div class="col-sm-2">
                <img src="<?php echo makeurl('/immagini/megafono.svg') ?>">
            </div>
            <div class="col-sm-10">
                Partecipa alla <a href="<?php echo makeurl('/partecipa') ?>">Call for Papers</a> e candida il tuo talk per il Linux Day Online!
            </div>
        </div>
    <?php endif ?>

    <div class="highlight-box mb-5">
        <strong><?php echo conf('human_date') ?></strong> torna la principale manifestazione italiana dedicata a Linux, al software libero, alla cultura aperta ed alla condivisione.
    </div>

    <?php if ($year == 2021): ?>
        <p class="lead">
            Dopo l'esperienza totalmente online del 2020, l'edizione 2021 del Linux Day si svolgerà sia dal vivo in alcune città che online: tanti talk in diretta, tante sessioni parallele, e la possibilità di entrare in contatto con appassionati, curiosi, professionisti e volontari in ogni parte d'Italia!
        </p>
    <?php else: ?>
        <p class="lead">
            Il Linux Day torna in presenza in tante città, e con l'edizione online per tutti gli altri: tanti talk in diretta, tante sessioni parallele, e la possibilità di entrare in contatto con appassionati, curiosi, professionisti e volontari in ogni parte d'Italia!
        </p>
    <?php endif ?>

    <div class="row mt-5 mb-5 align-items-center">
        <div class="col">
            <img class="img-fluid" src="<?php echo makeurl('/promo/?format=300x250') ?>">

            <?php if ($year == 2021): ?>
                <p class="mt-2">
                    <small><i>Il banner 2021 è opera<br>di Giuseppe "peppenna" Penna e del <a href="https://lugce.it/" rel="nofollow">LUGCE</a></i></small>
                </p>
            <?php endif ?>
        </div>
        <div class="col">
            <?php if ($year == 2021): ?>
                <p>
                    Dati, dati, dati... Ma dati a chi?
                </p>
                <p>
                    Il Linux Day 2021 è dedicato alle informazioni che alimentano il software, ed alle loro infinite fonti, rappresentazioni, trasformazioni e utilizzi: dati personali, dati pubblici, dati privati, dati rubati, dati liberi, big data, linked data, open data e dataset.
                </p>
                <p>
                    Per raccontare le esperienze virtuose, mettere in guardia sugli utilizzi meno virtuosi, esplorare strumenti e piattaforme che ne permettono la gestione, la tutela e la condivisione.
                </p>
            <?php elseif ($year == 2022): ?>
                <p>
                    Il tema del Linux Day 2022 è... undefined.
                </p>
                <p>
                    Davvero, non è uno scherzo: dopo due anni di pandemia e tante difficoltà, l'edizione 2022 è una tela bianca su cui poter scrivere ciò che si preferisce, ciò che più appassiona o più interessa, ciò che maggiormente coinvolge la propria community.
                </p>
            <?php endif ?>
        </div>
    </div>

    <hr>

    <p>
        Seguici su <a href="https://twitter.com/LinuxDayItalia">Twitter</a> (<a href="https://twitter.com/search?q=LinuxDay2023&f=live">#LinuxDay2023</a>) o <a href="https://www.facebook.com/LinuxDayItalia">Facebook</a>, o <a href="http://www.ils.org/newsletter">iscriviti alla newsletter</a> per aggiornamenti.
    </p>

    <p>
        L'accesso al Linux Day, sia fisico che virtuale, è libero e gratuito!
    </p>

<?php else: ?>
    <div class="highlight-box mb-5">
        <strong>🐧 <?php echo conf('human_date') ?></strong> torna il <b>Linux Day</b>: la principale manifestazione italiana dedicata al software libero, la cultura aperta ed alla condivisione!
    </div>
    <p>
        Il Linux Day nasce nel 2001 come appuntamento annuale per riunire le forze di tutte le persone attiviste nel movimento del <b>software libero</b>, dell'open source, ed in particolare di Linux.
        Proponiamo una rete di eventi decentralizzati in tutta Italia, organizzati autonomamente da gruppi di persone volontarie e appassionate.
        È il <b>più grande evento italiano</b> sul tema con migliaia di visitatori. Una tradizione da non perdere.
    </p>
    <p>
        Puoi trovare talk, workshop, spazi per l'assistenza tecnica, gadget, dibattiti e dimostrazioni pratiche e potrai incontrare la community!
    </p>
    <p>
        L'accesso al Linux Day è sempre libero e gratuito!
    </p>

    <?php if (hasBanner()): ?>
        <p class="text-center">
            <img class="banner" src="<?php echo makeurl('/promo/?format=300x250') ?>" alt="Linux Day <?php echo $year ?>">
        </p>
    <?php endif ?>

    <?php foreach(conf('theme') as $theme_line): ?>
        <p>
           <?php echo $theme_line ?>
        </p>
    <?php endforeach ?>
<?php endif ?>


<?php if(conf('is_physical')): ?>
    <?php if(count($events) === 0): ?>

    <h2>📍 Dove?</h2>

    <p>
    Quali città implementeranno un Linux Day? Scoprilo verso settembre...<br />oppure puoi aiutare in primis la tua community:
    </p>

    <p><a href="<?php echo makeurl('/howto/') ?>" class="btn btn-primary">Organizza il tuo Linux Day!</a></p>
    <p><a href="<?php echo makeurl('/condividi/') ?>" class="btn btn-secondary">Comunicato Stampa</a></p>

    <?php elseif(count($events) != 0): ?>
        <?php if(conf('is_virtual')): ?>
            <hr />
            <div id="local" class="mb-2 alert alert-info">
                Qui sotto trovi la mappa dei Linux Day locali che si svolgeranno <?php echo conf('human_date') ?>. Per partecipare all'edizione online del Linux Day, consulta <a href="<?php echo makeurl('/programma') ?>">questa pagina</a>.
            </div>
        <?php endif ?>

        <div id="mapid"></div>

        <script>
        $(function() {
            // Below this distance, you may have trouble distinguishing Free Circle and Sputnix.
            // Note that the exact distance between Free Circle and Sputnix is 1947 meters.
            // But let's be generous... lol
            var SUSPICIOUSLY_SMALL_DISTANCE_BETWEEN_LINUXDAYS = 2345.67890; // meters. asdlol

            // If you have this zoom or lesser, you probably can't distinguish Free Circle and Sputnix or similar situations...
            var SUSPICIOUSLY_LOW_ZOOM_LEVEL = 13;

            var linuxDays = [];
            var mymap = L.map('mapid', {
                gestureHandling: true
            }).setView([42.204, 11.711], 6);

            L.tileLayer('https://{s}.tile.openstreetmap.de/{z}/{x}/{y}.png', {
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
                maxZoom: 18,
            }).addTo(mymap);

            /**
             * Check if a marker overlaps with other markers.
             * @param {L.Marker} currentMarker
             * @param {array} markers
             */
            var isOverlappingWith = function(currentMarker, markers) {
                var overlaps = [];
                var currentLatLng = currentMarker.getLatLng();
                var marker, distance, i;
                for (i=0; i < markers.length; i++) {
                    marker = markers[i];
                    if (marker !== currentMarker) {
                        distance = currentLatLng.distanceTo(marker.getLatLng());
                        if (distance < SUSPICIOUSLY_SMALL_DISTANCE_BETWEEN_LINUXDAYS) {
                            //console.log("Wow. Two LinuxLays under this distance in meters: ", distance);
                            overlaps.push(marker);
                        }
                    }
                }
                return overlaps;
            }

            /**
             * Hook that prevents to open the popup of a specific marker,
             * if that marker is colliding with other ones.
             * This makes happy Free Circle and Sputnix :D :D
             * The "this" context is the currently clicked marker.
             * @param {L.MouseEvent}
             */
            var onMarkerClickEventuallyDisambiguate = function(e) {
                var overlap = isOverlappingWith(this, linuxDays);
                var latLngs = [], i;
                if (mymap.getZoom() < SUSPICIOUSLY_LOW_ZOOM_LEVEL && overlap.length) {
                    this.closePopup();
                    latLngs.push(this.getLatLng());
                    for (i = 0; i<overlap.length; i++) {
                        latLngs.push(overlap[i].getLatLng());
                    }
                    mymap.fitBounds(L.latLngBounds(latLngs));
                }
            };

            <?php foreach($events as $event): ?>
                <?php if(!empty($event->coords)): ?>
                    var m = L.marker([<?php echo $event->coords ?>], {closeOnClick: false, autoClose: false}).bindPopup(
                        '<b>' + <?php print_js_str(esc_html($event->group)) ?> + '</b></br>'+
                        <?php print_js_str(esc_html($event->city)) ?> + ' (' + <?php print_js_str(esc_html($event->prov)) ?> + ')</br>'+
                        '<a href="' + <?php print_js_str(esc_attr($event->web)) ?> + '">' + <?php print_js_str(esc_html($event->web)) ?> + '</a>'
                    ).addTo(mymap);
                    linuxDays.push(m);
                    m.on('click', onMarkerClickEventuallyDisambiguate);
                <?php endif ?>
            <?php endforeach ?>
        });
        </script>

        <?php if ($early_events): ?>
            <h3 class="mt-5 mb-3">🥂 Eventi di Riscaldamento al Linux Day</h3>
            <?php
            foreach ($early_events as $event) {
                $event->renderCard();
            }
            ?>
        <?php endif ?>

        <?php if ($normal_events): ?>
            <h3 class="mt-5 mb-3">🎉 Linux Day! <small><?= conf('human_date') ?></small></h3>
            <?php
            foreach ($normal_events as $event) {
                $event->renderCard();
            }
            ?>
        <?php endif ?>

        <?php if ($late_events): ?>
            <h3 class="mt-5 mb-3">🍻 Eventi successivi</h3>
            <?php
            foreach ($late_events as $event) {
                $event->renderCard();
            }
            ?>
        <?php endif ?>

        <div class="text-right mt-4">
            <p>Linux Day totali: <?= count( $events ) ?></p>
        </div>
    <?php endif ?>

    <?php
    $press_review = conf('press_review', []);
    if (!empty($press_review)) {
        // Ordinamento per data
        usort($press_review, function($a, $b) {
            return strtotime($a['date']) - strtotime($b['date']);
        });        
    ?>
        <h3>📰 Rassegna stampa</h3>

        <?php
        foreach ($press_review as $review) {
            echo '
            <div class="row press-review">
                <div class="col-10">
                    🔗 <a href="'.makeurl($review['url']).'" target="_blank" rel="nofollow">'.$review['title'].'</a>
                </div>
                <div class="col-2 text-right text-muted">
                    '.date('d/m/Y', strtotime($review['date'])).'
                </div>
            </div>
            ';
        }
        ?>
        <br><br>
    <?php } ?>

    <h3>🤳 Condividi i momenti migliori</h3>

    <p>
        Segui l'evento sul social network libero <b>Mastodon</b><br />(accessibile con qualsiasi piattaforma ActivityPub):<br />
        <a href="https://mastodon.uno/@ItaLinuxSociety">@ItaLinuxSociety @<b>Mastodon.uno</b></a>
    </p>
    <p>oppure seguici su social tradizionali come:<br />
        <a href="https://twitter.com/LinuxDayItalia">@LinuxDayItalia in <b>Twitter</b></a> (<a href="https://twitter.com/search?q=LinuxDay<?php echo $year ?>&f=live">#LinuxDay<?php echo $year ?></a>)<br />
        <a href="https://www.facebook.com/LinuxDayItalia">@LinuxDayItalia in <b>Facebook</b></a>
    </p>
    <p>Oppure iscriviti alla newsletter per aggiornamenti futuri!<br />
       <a href="https://www.ils.org/newsletter">iscriviti alla newsletter</a>
    </p>

    <p>Usa l'hashtag ufficiale #LinuxDay<?php echo $year ?> e le foto migliori saranno ricondivise!</p>

<?php endif ?>

<?php
lugfooter ();
