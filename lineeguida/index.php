<?php
/*
Copyright (C) 2019-2024  Italian Linux Society e contributori https://www.linux.it

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once ('../funzioni.php');
lugheader ('Linux Day ' . conf('current_year') . ': Linee Guida');

?>

<h2>Linee Guida</h2>

<p>
	Versione sintetica, per chi ha fretta.
</p>

<ul>
	<li>Il Linux Day <?php echo conf('current_year') ?> si terrà <?php echo strtolower(conf('human_date')) ?></li>
	<li>Il Linux Day è una manifestazione nazionale unitaria articolata in varie manifestazioni locali</li>
	<li>Essendo volto a favorire un'ampia diffusione e conoscenza di GNU/Linux e del software libero, il Linux Day si rivolge principalmente al grande pubblico</li>
	<li>Il Linux Day ha lo scopo di promuovere l'uso e la conoscenza del sistema operativo Linux e del software libero</li>
	<li>L'accesso alla manifestazione deve sempre essere libero e gratuito</li>
	<li>Agli organizzatori locali è lasciata la possibilità di eventuali raccolte fondi</li>
</ul>

<p>
	Versione estesa, per chi vuole saperne di più.
</p>

<ol>
	<li><b>Scopo</b><br />
		Il Linux Day ha lo scopo di promuovere l'uso e la conoscenza del software libero e del sistema operativo GNU/Linux. È eventualmente possibile prendere in considerazione anche altri sistemi operativi liberi, purché la manifestazione sia comunque incentrata su GNU/Linux. Il fine è la promozione del software libero, e non la promozione di qualunque programma che giri su GNU/Linux; è comunque possibile accennare marginalmente a software proprietario relativo a GNU/Linux per completezza di informazione, ad esempio per questioni di interoperabilità, o in mancanza di un equivalente libero eccetera, purché tale accenno sia motivato rispetto allo scopo della manifestazione e non vi sia assolutamente l'intento di promuovere tale software. È inoltre possibile accennare alla compatibilità con sistemi proprietari relativamente a software libero disponibile, oltre che per GNU/Linux, anche per tali sistemi. In ogni caso la responsabilità di ciò che viene mostrato, se non fa parte dei materiali comuni, ricade esclusivamente sul singolo gruppo locale (ad esempio per questioni di licenze) e non su ILS o sui restanti organizzatori.
	</li>
	<li><b>Pubblico</b><br />
		Essendo volto a favorire un'ampia diffusione e conoscenza del software libero e di GNU/Linux, il Linux Day si rivolge principalmente al grande pubblico. Questo non esclude la possibilità di trattare argomenti avanzati e specialistici, tuttavia è necessario non trascurare attività e interventi destinati agli utenti meno esperti, sia per quanto riguarda gli aspetti tecnici che il concetto stesso di software libero.
	</li>
	<li><b>Accesso</b><br />
		L'accesso alla manifestazione deve sempre essere libero e gratuito, in ogni sua parte e momento. Non sono ammesse forme di ingresso a pagamento o dietro iscrizioni obbligatorie, qualunque sia il gruppo o associazione interessato.
	</li>
	<li><b>Neutralità</b><br />
		L'organizzazione della manifestazione non deve essere in alcun modo legata ad attività di gruppi politici, religiosi o di qualunque altro tipo che non perseguano le finalità della manifestazione stessa. Non è quindi accettabile che tali soggetti facciano parte dell'organizzazione, che ospitino la manifestazione (o parte di essa) all'interno di proprie iniziative o collegandola con proprie iniziative e attività, o che intervengano durante la stessa se non per perseguire gli obiettivi della manifestazione come esposti al punto 1. Questo non esclude eventuali rapporti con figure istituzionali.
	</li>
	<li><b>Raccolta fondi</b><br />
		Agli organizzatori locali è lasciata la possibilità di eventuali raccolte fondi, ad esempio con offerte volontarie, vendita di gadget o raccolta di nuove iscrizioni, da gestire in proprio. Qualunque responsabilità in tal senso, ad esempio per questioni fiscali, ricade sul relativo gruppo locale e non sui restanti organizzatori. Eventuali gadget o materiali forniti gratuitamente a livello nazionale tramite ILS per la distribuzione durante la manifestazione devono comunque essere distribuiti gratuitamente: non possono quindi essere venduti, nè è possibile distribuirli in cambio di un'offerta o dell'iscrizione a un'associazione.
	</li>
	<li><b>Sponsor</b><br />
		È ammessa la possibilità di sponsorizzazioni della manifestazione, sia a livello nazionale che per i singoli eventi locali, con le medesime regole. L'eventuale presenza degli sponsor, sia nazionali che locali, deve comunque essere in linea con gli obiettivi della manifestazione come esposti al punto 1 e deve essere salvaguardato il carattere non commerciale, plurale e indipendente della manifestazione stessa. Al fine di evitare che un evento locale sia focalizzato sulla presentazione di una particolare azienda e/o dei suoi prodotti e servizi, il che non è ammissibile, dovranno essere rispettati i seguenti punti:
		<ul>
			<li>sono ammesse donazioni in denaro, da qualsiasi ente o azienda, all'organizzatore;</li>
			<li>sono ammesse donazioni in materiale attinente al software libero (cdrom, libri, documentazione, gadget, magliette, penne, cartelline...);</li>
			<li>non è ammessa la distribuzione di materiale che non riguardi il software libero o contenuti liberi;</li>
			<li>uno sponsor non può cedere ad altri enti, aziende o marchi i diritti derivanti dalla sponsorizzazione dell'evento senza prima aver interpellato gli organizzatori (locali o nazionali) di riferimento.</li>
		</ul>
		I gruppi locali che hanno bisogno di aiuto per attivare una sponsorizzazione in modo legale, possono ricondursi alle <a href="https://www.ils.org/sponsorizzazioni/">sponsorizzazioni Italian Linux Society</a>.
	</li>
	<li><b>Identità dell'evento</b><br />
		Il Linux Day è una manifestazione nazionale unitaria articolata in varie manifestazioni locali. È necessario che l'aspetto unitario sia evidente e venga sottolineato, utilizzando sempre e solo la denominazione ufficiale "Linux Day". Esempi di varianti corrette:
		<ul>
			<li>Linux Day</li>
			<li>Linux Day <?php echo conf('current_year') ?></li>
			<li>Linux Day [NomeCittà]</li>
			<li>Linux Day [NomeCittà] <?php echo conf('current_year') ?></li>
			<li>LD<?php echo substr(conf('current_year'), 2, 2) ?></li>
		</ul>
		Il Linux Day non è intitolato "FreeBSD Day", "GNU Day", "GNU/Linux Day", "GNU+Linux Day" "Open Day" o altre varianti per un semplice motivo: l'evento è consolidato dal 2001 e sarebbe inopportuno cambiargli nome oggi.
		In ogni caso "Linux Day" è un titolo semplice e altisonante che attrae persone anche non necessariamente legate al mondo del software libero, al fine di portare quei temi anche a queste persone.
		In ogni caso i vari user-group possono tranquillamente scegliere il "sotto-titolo" che più preferiscono fra cui:
		<ul>
			<li>giornata nazionale del software libero e di GNU/Linux</li>
			<li>giornata nazionale del software libero e di GNU+Linux</li>
			<li>giornata nazionale del software libero e dei sistemi operativi liberi</li>
			<li>giornata nazionale del software libero e delle libertà digitali</li>
		</ul>
		O altre sfaccettature coerenti con lo scopo al punto 1.<br />
		Il sito web dell'evento deve riportare le informazioni necessarie (data e luogo) in modo chiaro, inserendo anche un riferimento al sito web nazionale www.linuxday.it, e potendo utilizzare logo, manifesto, volantini e altro materiale comune messi a disposizione.<br />
		Il sito web dell'evento dovrebbe essere in linea con lo spirito stesso dell'evento, e dovrebbe quindi evitare diciture come "tutti i diritti riservati", non dovrebbe contenere pubblicità, non deve contenere software proprietario (come Google Analytics o simili).
	</li>
	<li><b>Data</b><br />
		Il Linux Day <?php echo conf('current_year') ?> si terrà <?php echo strtolower(conf('human_date')) ?>. Ciascun gruppo organizzatore ha facoltà di organizzare anche eventi collaterali legati al Linux Day nei giorni dal giovedì precedente alla domenica successiva, purché l'evento principale si tenga <?php echo strtolower(conf('human_date')) ?>. L'evento principale e quelli collaterali ad esso legati possono anche svolgersi in località vicine; è comunque necessario che il gruppo organizzatore sia unificato e che gli eventi vengano gestiti e pubblicizzati in maniera unitaria e secondo un programma unico. Anche gli eventi collaterali devono essere conformi alle linee guida qui specificate.
	</li>
	<li><b>Programma</b><br />
		Affinché il proprio evento locale sia incluso nella manifestazione è necessario darne comunicazione sufficientemente dettagliata, includendo il proprio programma di massima, a ILS entro e non oltre il 10 ottobre <?php echo conf('current_year') ?>. ILS si riserva la facoltà di non includere eventi che, a proprio giudizio, appaiano non conformi con lo spirito della manifestazione e con le presenti linee guida, nonché eventi organizzati da gruppi che, in precedenti edizioni, abbiano organizzato eventi non conformi con lo spirito della manifestazione.
	</li>
	<li><b>Relatori e Contenuti</b><br />
		Gli organizzatori informano i relatori sulle buone e cattive pratiche, affinché ci sia ogni accorgimento possibile per evitare la promozione di software proprietario, anche indirettamente, durante una presentazione. Esempi non accettabili:
		<ul>
			<li>presentare da un computer che ha sopra un logo molto riconoscibile di una nota società di software proprietario (esempio di soluzione: coprire quel logo con un adesivo libero / usare un altro computer / ecc.)</li>
			<li>avere una presentazione visibilmente in formato proprietario e/o con software proprietario (esempio di soluzione: usare LibreOffice Impress / usare un altro computer / ecc.)</li>
			<li>proiettare il proprio schermo con icone di software proprietario sparse sul desktop o nella toolbar del browser (esempio di soluzione: rimuovere quelle icone / creare un altro account pulito / usare un altro computer / ecc.)</li>
		</ul>
		Per quanto non siamo in televisione, è comunque buona cosa evitare di promuovere marchi in generale (esempio: preferire acqua senza etichetta, abbigliamento non griffato, ecc.).<br />
		Chi presenta non deve violare le leggi, fra cui il diritto d'autore.<br />
		Chi presenta dà il giusto credito ai materiali utilizzati (esempio: crediti alla fine delle proprie slide)<br />
		Ottimo se i relatori rilasciano i loro nuovi contenuti prodotti (testi delle presentazioni, immagini, ecc.) usando una fra le seguenti licenze di contenuto libero: <a href="https://creativecommons.org/publicdomain/zero/1.0/">CC0</a>, <a href="https://creativecommons.org/licenses/by/4.0/">CC BY</a> o <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.it">CC BY-SA</a>. Ottimo se il logo della propria licenza libera è in evidenza, per esempio all'inizio delle slide.
	</li>
	<li><b>Fotografie</b><br />
		Solitamente ai Linux Day si fanno molte fotografie e video quindi si ricorda che normalmente non è possibile diffondere fotografie di minori. Questo vale in generale anche per le persone, se non si ha la loro autorizzazione.
		Per semplificare la vita a chi fa foto, è consigliabile che all'ingresso dell'evento sia chiara l'approvazione all'essere fotografati.
		Chi non desidera essere pubblicato in una fotografia dovrebbe avere la possibilità di indicarlo visibilmente agli altri partecipanti (per esempio con una spilla "no foto").
		Nel programma della giornata, si consiglia di indicare un momento per la foto di gruppo.
	</li>
	<li><b>Inclusione e rispetto</b><br />
		Ottimo se gli organizzatori indicano sul sito e all'ingresso dell'evento un codice di condotta a cui i partecipanti devono attenersi per poter partecipare, e quindi per poter allontanare chi pratica comportamenti molesti, fra cui commenti verbali offensivi, o discriminazione relative alle preferenze sulla propria distribuzione GNU/Linux, discriminazione in base alla propria licenza libera preferita, oppure discriminazione per orientamento sessuale, razza, religione, disabilità; i partecipanti non devono fare uso inappropriato della nudità, intimidazione deliberata, stalking, pedinamento; fotografie o registrazioni moleste; disturbo prolungato delle sessioni; contatto fisico non richiesto; attenzioni sessuali indesiderate. Grazie!
	</li>
	<li><b>Ma soprattutto</b><br />
		Ma soprattutto: non dimenticate di sorridere e divertirvi, festeggiando insieme alla vostra bella community! Buon Linux Day!
	</li>
</ol>

<hr />

<p>Hai appena letto le linee guida. Abbiamo anche alcune informazioni pratiche:</p>
<p><a href="<?php echo makeurl('/howto/') ?>" class="btn btn-secondary">Organizza il tuo Linux Day</a></p>

<?php
lugfooter ();

