<?php
if( !function_exists('conf') ) {
	exit; // Silence is golden
}
?>
    <div class="card linux-day-event-card">
       <div class="card-body">
            <small class="float-md-right"><?= esc_html($event->getDateLocalizedFormat('j F')) ?></small>
            <h5 class="card-title linux-day-event-title"><a href="<?php echo esc_attr($event->web) ?>" title="<?= esc_attr( sprintf( "Sito ufficiale del Linux Day %s - realizzato da %s", $event->city, $event->group ) ) ?>"><?php
                echo "🐧"; // This is a penguin emojii
                echo " ";
                echo $event->getHTMLTitleNice();
            ?></a></h5>
            <div><small>Organizzato da <em><?php echo esc_html($event->group) ?> (<?= $event->prov ?>)</em></small></div>

            <?php if ($event->isPaid()): ?>
            <div role="alert" class="mt-1">
               <details>
                   <summary>Come entrare in questo specifico evento</summary>
                   <p>Attenzione: diversamente da tutti gli altri Linux Day,
                      lo specifico evento <?= $event->getTitleNice() ?> prevede un <b>costo all'ingresso</b>.
                      Le persone volontarie che organizzano questo evento, <?php echo esc_html($event->group) ?>,
                      non sono gli organizzatori della fiera e non traggono alcun vantaggio economico
                      e partecipano a questa fiera soltanto al fine di portare software libero in quella fiera.
                      Se vuoi accedere a questo stand, quindi, informati sui costi della fiera seguendo i consigli del sito <a href="<?= esc_attr($event->web) ?>"><?= $event->getTitleNice() ?></a>.
                   </p>
                   <p>Abbiamo promesso che il Linux Day sarà sempre gratuito.
                   Quindi se sei <a href="https://ilsmanager.linux.it/ng/register" target="_blank">associato a Italian Linux Society</a> (anche da poco) potrai <a href="https://www.ils.org/info/rimborsi/" target="_blank">chiedere un rimborso</a> del tuo singolo biglietto personale (fino ad un massimo assoluto di 25 euro), soprattutto se hai aiutato chi organizza o se hai scattato delle belle fotografie libere sui social!
                   Nella tua richiesta, allega il tuo biglietto nominale. Grazie!</p>
               </details>
            </div>
            <?php endif ?>
        </div>
    </div>
