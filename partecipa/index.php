<?php

/*
Copyright (C) 2019  Italian Linux Society - http://www.linux.it

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once ('../funzioni.php');

if (conf('is_virtual') == false) {
    header('Location: /');
    exit();
}

$year = conf('current_year');

lugheader ('Linux Day ' . $year . ': Candida Talk');

$is_admin = isset($_SESSION['admin']) && $_SESSION['admin'] == 'S';

$past_save_date = date('Y-m-d') > conf('computer_date');

if (!isset($_POST['challenge'])) {
    $first = rand(1, 10);
    $second = rand(1, 10);
    $challenge = sprintf('%d + %d', $first, $second);
    $_SESSION['challenge'] = (string) ($first + $second);
}
else {
    $talks_file = '../data/talks' . $year . '.json';

    $challenge = $_POST['challenge'];
    if ($_SESSION['challenge'] != trim($challenge)) {
        $message = "<b>ERRORE:</b> Captcha non valido.";
    }
    else {
        $talk = (object) [
            'date' => date('Y-m-d H:i:s'),
            'ip' => $_SERVER['REMOTE_ADDR'] ?? '',
            'name' => ucwords(trim(strip_tags($_POST['name']))),
            'email' => strtolower(trim(strip_tags($_POST['email']))),
            'talk' => trim(strip_tags($_POST['talk'])),
            'slot' => trim(strip_tags($_POST['slot'])),
            'abstract' => trim(strip_tags($_POST['abstract'])),
            'notes' => trim(strip_tags($_POST['notes'])),
            'mode' => trim(strip_tags($_POST['mode'])),
            'approved' => false,
        ];

        $result = saveTalk($talks_file, $talk);
        $message = "Talk salvato correttamente! Ti aggiorneremo sullo stato di approvazione all'indirizzo " . $talk->email;
    }
}

?>

<h2>Candida Talk</h2>

<?php if(!empty($message)): ?>
    <div class="alert alert-warning">
        <?php echo $message ?>
    </div>
<?php endif ?>

<p class="alert alert-info">
    Da questa pagina puoi candidare un talk alla manifestazione online che si svolgerà <?php echo conf('human_date') ?>. <strong>La deadline per l'invio delle candidature è <?php echo conf('human_talks_date') ?>.</strong><br><br>
    <?php if ($year == 2021): ?>
        Il tema di quest'anno sono "i dati", dunque particolarmente apprezzati e benvenuti sono interventi relativi a strumenti e pratiche per la tutela dei dati personali, modellazione e utilizzo di dataset per il machine learning, open data ed informazioni strutturate, e altri argomenti similari. Tali spunti non sono comunque vincolanti, e vengono accolte tutte le candidature riconducibili al software ed alla cultura liberi.<br><br>
    <?php elseif ($year == 2022): ?>
        Il tema di quest'anno è "undefined", dunque puoi sbizzarrirti come più ti piace: vengono accolte tutte le candidature riconducibili al software ed alla cultura liberi.<br><br>
    <?php endif ?>
    I relatori selezionati potranno svolgere la propria presentazione in diretta streaming o inviare una registrazione audio/video che sarà resa pubblica nel giorno e nell'ora stabiliti dal programma. In entrambi i casi sarà possibile confrontarsi con il pubblico via chat nel corso dell'intervento.<br><br>
    Nota bene: se intendi sottoporre la tua candidatura per partecipare di persona ad un Linux Day fisico <strong>non compilare</strong> questo form, ma cerca maggiori informazioni sul sito dell'organizzatore locale. Questa pagina fa riferimento solo al Linux Day <?php echo $year ?> Online.<br><br>
    Per segnalazioni, problemi e dubbi, scrivi <a href="https://forum.linux.it/t/call-for-papers-linux-day-online-2021/201">sul Forum ILS</a> o manda una mail a <a href="mailto:webmaster@linux.it">webmaster@linux.it</a>.
</p>

<form method="POST" action="<?php echo makeurl('/partecipa/index.php') ?>">
    <div class="form-group">
        <label for="name">Nome e Cognome</label>
        <input type="text" class="form-control" id="name" name="name" maxlength="50" required>
    </div>
    <div class="form-group">
        <label for="email">E-Mail</label>
        <input type="email" class="form-control" id="email" name="email" maxlength="50" required>
        <small class="form-text text-muted">Sarai ricontattato a questo indirizzo: controlla di averlo scritto correttamente!</small>
    </div>
    <div class="form-group">
        <label for="slot">Durata</label>
        <select class="form-control" id="slot" name="slot" required>
            <option>Seleziona una opzione</option>
            <option value="30">30 minuti</option>
            <option value="60">60 minuti</option>
        </select>
    </div>
    <div class="form-group">
        <label for="talk">Titolo del Talk</label>
        <input type="text" class="form-control" id="talk" name="talk" maxlength="50" required>
        <small class="form-text text-muted">50 caratteri. Titolo pubblico del tuo talk.</small>
    </div>
    <div class="form-group">
        <label for="abstract">Abstract</label>
        <textarea class="form-control" id="abstract" name="abstract" rows="7" minlength="100" maxlength="2000" required></textarea>
        <small class="form-text text-muted">Minimo 100 caratteri, massimo 2000 caratteri. Descrizione pubblica del tuo talk.</small>
    </div>
    <div class="form-group">
        <label for="abstract">Modalità Preferita</label>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="mode" id="mode_live" value="live" checked>
            <label class="form-check-label" for="mode_live">Intervento live</label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="mode" id="mode_video" value="video">
            <label class="form-check-label" for="mode_video">Video pre-registrato</label>
        </div>
        <small class="form-text text-muted">Questa indicazione non è vincolante, potrai cambiare idea successivamente.</small>
    </div>
    <div class="form-group">
        <label for="notes">Note per l'Organizzazione</label>
        <textarea class="form-control" id="notes" name="notes" rows="7" maxlength="2000"></textarea>
        <small class="form-text text-muted">2000 caratteri. Eventuali note e segnalazioni per gli organizzatori. Non saranno pubblicate.</small>
    </div>
    <div class="form-group">
        <label for="challenge"><?php echo $challenge ?></label>
        <input type="text" class="form-control" id="challenge" name="challenge" required>
        <small class="form-text text-muted">Tu sai perché ;-)</small>
    </div>
    <div class="form-group form-check">
        <input type="checkbox" class="form-check-input" id="privacy" name="privacy" required>
        <label class="form-check-label" for="privacy">Ho letto l'<a href="http://www.ils.org/privacy" target="_blank">Informativa Privacy</a></label>
    </div>

    <button type="submit" class="btn btn-primary" <?php echo($past_save_date ? 'disabled' : '') ?>>Salva</button>
</form>

<?php
lugfooter ();
