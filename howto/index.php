<?php
/*
Copyright (C) 2019-2024  Italian Linux Society, Linux Day website contributors - https://ils.org/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once ('../funzioni.php');

if (conf('is_physical') == false) {
	header('Location: /');
	exit();
}

lugheader ('Linux Day ' . conf('current_year') . ': Organizza');

?>

<h2>Organizza il tuo Linux Day</h2>

<p>
	Nella tua zona nessuno organizza un Linux Day? E allora, fallo tu!
</p>

<p>
	Per organizzare un Linux Day non è necessario essere un Linux User Group, una associazione o altro: basta qualche amicizia, un pò di buona volontà, e accettare alcuni semplici consigli e raccomandazioni.
</p>

<ol>
	<li>
		<p>Incontra l'user group più vicino a te, per esempio, partendo dalla mappa dei Linux User Group (LugMap).<br />
		   Magari di fronte ad una bella pizza si ragiona meglio per organizzare un Linux Day!
                </p>
		<p><a href="https://lugmap.linux.it/" class="btn btn-secondary">LugMap</a></p>
	</li>
	<li>
		<p>Leggete le linee guida del Linux Day: queste sono le uniche regole che occorre seguire per aderire al Linux Day: la data è quella di <b><?php echo strtolower(conf('human_date')) ?></b>, l'accesso del pubblico deve essere gratuito, e tra i temi trattati non devono apparire applicazioni proprietarie (neanche se girano su Linux!) o comunque argomenti opposti allo spirito di condivisione e apertura della community che intendete rappresentare.
                   È ovviamente possibile parlare di software proprietario <u>se</u> è chiaro che ci sia uno spirito palesemente critico.
                </p>
		<p><a href="<?php echo makeurl('/lineeguida/') ?>" class="btn btn-secondary">Linee Guida Linux Day</a></p>
	</li>
	<li>
		<p>Trovate <strong>una sede</strong>. Questa è probabilmente la parte più complessa dell'organizzazione, in quanto per allestire qualsiasi attività servono almeno uno o due locali per <?php echo strtolower(conf('human_date')) ?> e non sempre si trovano gratuitamente o a prezzo accessibile.
                   Sicuramente si può iniziare chiedendo nelle scuole e nelle sedi universitarie, o valutare qualche sala del Comune ma sono anche apprezzate soluzioni creative come gazebo o aree coperte nelle piazze (occhio a chiedere l'autorizzazione di occupazione suolo pubblico in Comune!), locali pubblici (nessuno vieta di "occupare" amichevolmente la birreria o il ristorante di un amico, purché come detto sopra non ci siano obblighi di spesa per il pubblico), aree all'interno dei centri commerciali (previa ovvia autorizzazione)...
                   I limiti sono la fantasia e l'audacia!
		</p>
	</li>
	<li>
		<p>Definite dei <strong>contenuti</strong>.
                   Molto dipende da quanti e quali spazi avete trovate nella fase precedente (una sala? Due? Sei?) e da quante persone sono coinvolte nel vostro gruppo.
                   Il format più comune è quello dei talk, percui sono necessari relatori che sappiano a turno illustrare i vari temi scelti ("Cos'é il Software Libero", "Panoramica su Linux", "Funzioni Avanzate di LibreOffice", o quel che volete) nonché la disponibilità di sedie e magari uno o più proiettori per le slide.
                   Ma non è l'unica modalità: c'è chi, avendo disponibilità di qualche computer, allestisce banchetti tematici ed interattivi presso cui è possibile circolare e soffermarsi per maggiori informazioni (e.g. progetti di elettronica, videogiochi opensource, dimostrazioni di programmi liberi per la grafica o l'editing audio/video, un paio di postazioni Linux per metterci direttamente le mani...), o più semplicemente si può allestire un punto informativo presso cui distribuire materiali, gadgets, assistenza ed informazioni (opzione particolarmente azzeccata se vi trovate in un luogo di passaggio, come appunto un gazebo in piazza o un centro commerciale).
                </p>
	</li>
	<li>
		<p>
		   Pubblicate una <atring>pagina web</strong> dedicata all'evento, su cui fornire i dettagli indispensabili alla partecipazione: orari, indirizzo preciso della sede, programma... Prendete ispirazione ad esempio dai siti dei Linux Day passati per capire cosa può essere utile comunicare, e non dimenticate di esporre il logo ufficiale:
                </p>
		<p><a href="<?php echo makeurl('/immagini/linuxday_fullcolor.svg') ?>" class="btn btn-secondary" target="_blank">Logo Ufficiale Linux Day vettoriale</a></p>
	</li>
	<li>
		<p><strong>Registrate</strong> il vostro evento su linuxday.it per essere ammessi all'indice nazionale delle iniziative:
		   questo è probabilmente il canale preferenziale per entrare in contatto con il potenziale pubblico:
		</p>
		<p><a href="<?php echo makeurl('/user/') ?>" class="btn btn-secondary" target="_blank">1. Registra Utenza Organizzatrice su LinuxDay.it</a></p>
                <p><a href="<?php echo makeurl('/registra/') ?>" class="btn btn-secondary" target="_blank">2. Registra Evento su LinuxDay.it</a></p>
	</li>
	<li>
		Fate <strong>promozione</strong>. La pubblicazione su linuxday.it non basta, e mai bisogna dare per scontato che le persone sappiano che ci sia il Linux Day: mandate un comunicato ai giornali locali, stampate qualche volantino da appendere nelle bacheche pubbliche, scrivete ai rappresentanti locali, insomma fatevi vedere!
	</li>
	<?php if( conf('current_year') >= 2024 ): ?>
	<li>
		<p>
		   Diffondete e adattate un comunicato stampa ai vostri contatti stampa locali, menzionando gli argomenti chiave del vostro Linux Day e eventuali invitati speciali o altre informazioni ghiotte per la stampa.
                </p>
		<p><a href="<?php echo makeurl('/condividi/') ?>" class="btn btn-secondary">Comunicato stampa Linux Day</a></p>
	</li>
	<?php endif ?>
</ol>

<h3>Durante l'evento</h3>
<ul>
	<li>
		<p><b>Info Fotografie</b><br />
		   Alla plenaria di apertura dell'evento investite 10 secondi per ricordare l'hashtag #LinuxDay<?= conf('current_year') ?>,
		   e ricordate che è normale siano fatte foto da chiunque, e a chiunque... quindi informate chi <b>non</b> desidera finire in fotografie
		   di segnalarlo in modo chiaro, magari usando gli appositi adesivi da cartoleria che avete fornito all'ingresso,
		   su cui poter scrivere "no foto" per attaccarselo addosso (o qualsiasi altra cosa di semplice ed efficace abbiate previsto
		   per venire incontro a questa semplice necessità sempre più frequente - soprattutto nei grandi eventi).
		</p>
	</li>
	<li>
		<p><b>Assistenza installazione Linux</b><br />
		   Aspettatevi che arrivino delle persone con il proprio computer per avere aiuto nell'installazione di Linux, o per risolvere qualche problematica tecnica specifica: portatevi qualche chiavetta USB avviabile per l'installazione.
		   Preparate qualche cartello che indichi la presenza di questa assistenza gratuita, da appendere nella vostra zona per rendervi più evidenti alle persone di passaggio.
		   Fate sapere alle persone che la vostra community locale esiste, e si incontra ogni tanto, e dove.
		   Fate sapere che c'è anche un sito web di assistenza tecnica gratuita nazionale (sempre gestita da persone volontarie).
		   Fate sapere che ci sono anche negozi in tutta Italia che fanno assistenza su Linux e spesso offrono anche contratti di assistenza per uffici o aziende (quindi non necessariamente è tutto sul carico di persone volontarie).
		</p>
		<p>
		   <a href="https://www.linux.it/contatti/" class="btn btn-secondary">Contatti Assistenza Linux.it</a>
		</p>
	</li>
	<li>
		<p><b>Foto di gruppo</b><br />
		   Nel programma, annunciate l'orario esatto della foto di gruppo a fine evento!
		   Cogliete l'occasione per ricordare a chi ha il badge "No foto" di evitare di piantarsi davanti alla macchina foto, se non vogliono finire comunque in una foto.
		</p>
 	</li>
	<li>
		<?php echo strtolower(conf('human_date')) ?>, <strong>divertitevi</strong> il più possibile!
	</li>
</ul>

<h3>Dopo l'evento</h3>
<ul>
	<li>
		Condividete sui vostri social le foto migliori. Assicuratevi di menzionare chi ha scattato la foto e aggiungete "foto rilasciata da Nome Cognome in licenza "CC BY SA 4.0" (o altra licenza libera). Assicuratevi di non condividere fotografie di minori (troppo complicate da rilasciare) e di avere il consenso delle persone ritratte (come sempre).
	</li>
	<li>
		se siete soddisfatti della vostra opera, valutate la possibilità di costituirvi in <strong>LUG</strong> permanente. Tutti i dettagli sul come e perché, e le opportunità di assistenza, li potete trovare nel <a href="https://www.linux.it/manuale/">Manuale Operativo per la Community</a> pubblicato da Italian Linux Society
	</li>
</ul>

<p>
	Qualcosa non è ancora chiaro? Serve maggiore supporto o chiarimenti? Non sapete dove pubblicare la pagina web per il vostro evento? Per qualsiasi informazione o richiesta potete scrivere a <a href="mailto:webmaster@linux.it">webmaster@linux.it</a>.
</p>

<p>
	E buon Linux Day!
</p>

<hr />

<p>Hai appena letto le informazioni pratiche. Puoi rileggere anche le linee guida:</p>
<p><a href="<?php echo makeurl('/lineeguida/') ?>" class="btn btn-secondary">Linee Guida</a></p>

<?php
lugfooter();
