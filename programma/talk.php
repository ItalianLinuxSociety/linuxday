<?php
/*
Copyright (C) 2020  Italian Linux Society - http://www.linux.it

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once ('../funzioni.php');

$slug = $_GET['slug'] ?? '';
$talk = findTalk($days_file, $slug);
if (is_null($talk)) {
    header('Location: index.php');
}

lugheader('LD' . conf('current_year') . ' | ' . $talk->talk);

?>

<a href="index.php" class="btn btn-sm btn-info mb-5">Torna al programma completo</a>

<h2><?php echo $talk->talk ?></h2>
<hr>
<h4><?php echo $talk->name ?></h4>
<hr>
<h6><?php echo sprintf('%s - %s - ore %s', $talk->session, $talk->day, $talk->hour) ?></h6>

<hr>

<p>
    <?php echo nl2br($talk->abstract) ?>
</p>

<hr>

<!--
<a class="btn btn-warning float-right" href="<?php echo makeurl('programma/live.php?slug=' . $talk->session) ?>"><?php echo findSession($talk->session)->label ?><br><small>Clicca qui per accedere</small></a>
-->

<?php if (!empty($talk->video)): ?>
    <div style="position:relative;padding-top:56.25%;clear:both">
        <iframe style="position:absolute;top:0;left:0;width:99%;height:99%;" sandbox="allow-same-origin allow-scripts allow-popups" src="<?php echo str_replace('watch', 'embed', $talk->video) ?>" frameborder="0" allowfullscreen></iframe>
    </div>

    <hr>
<?php endif ?>

<?php if (!empty($talk->video)): ?>
    <a class="btn btn-warning float-right" href="<?php echo $talk->video ?>" target="_blank">Guarda il Video</a>
<?php endif ?>

<?php if (!empty($talk->slides)): ?>
    <?php if (strncmp($talk->slides, 'http', 4) == 0): ?>
        <a class="btn btn-warning float-right" href="<?php echo $talk->slides ?>">Scarica la Presentazione</a>
    <?php else: ?>
        <a class="btn btn-warning float-right" href="<?php echo makeurl($talk->slides) ?>">Scarica la Presentazione</a>
    <?php endif ?>
<?php endif ?>

<?php

lugfooter();
