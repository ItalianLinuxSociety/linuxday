<?php
/*
Copyright (C) 2022  Italian Linux Society - http://www.linux.it

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once ('../funzioni.php');

filledpageheader('Linux Day ' . conf('current_year') . ' | Programma');

?>

<p class="lead">
    Qui di seguito il programma del Linux Day Online <?php echo conf('current_year') ?>. Clicca sui titoli dei talk per maggiori informazioni.<br>
    Torna su questa pagina <?php echo conf('human_date') ?> per accedere alla diretta streaming.
</p>
<p>
    Per informazioni sui Linux Day cittadini, consulta le relative <a href="<?php echo makeurl('/#local') ?>">pagine web enumerate in homepage</a>.
</p>

<?php

include('timetable.php');

lugfooter();
